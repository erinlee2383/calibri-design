<div id="site-header">
        <div id="site-header-image">
            <a href="/">
                <img src="<?php print path_to_theme(); ?>../imgs/banner.jpg" />
            </a>
        </div>
        <div class="container top">
            <div id="logo">
                <img src="<?php print path_to_theme(); ?>../imgs/logo.png" title="Home" alt="Calibri Design" />
            </div>
        </div>
    </div>
    <div class="container">
        <div class="menu-nav-container main">
            <?php 
                print theme('links',array('links'=>$main_menu));
            ?>
        </div>
    </div>
    <div class="container content">
        <div class="clearfix">
            
            <h1><?php print $title; ?></h1>

            <?php if ($messages): ?>
                <div id="messages"><div class="section clearfix">
                  <?php print $messages; ?>
                </div></div> <!-- /.section, /#messages -->
            <?php endif; ?>
            
            <?php if ($tabs): ?>
                <div class="tabs">
                    <?php print render($tabs); ?>
                </div>
            <?php endif; ?>

            <?php if($page['left_callout']): ?>
                <div class="<?php print $variables['widthClasses']['column'] ?>">
                    <?php print render($page['left_callout']); ?>
                </div>
            <?php endif; ?>
            
            <div class="<?php print $variables['widthClasses']['main'] ?>">
                <?php
                    print render($page['content']);
                ?>
            </div>
            
            <?php if($page['right_callout']): ?>
                <div class="<?php print $variables['widthClasses']['column'] ?>">
                    <?php print render($page['right_callout']); ?>
                </div>
            <?php endif; ?>
            
        </div>
    </div>
    <div id="site-footer">
        <div class="container green">
            <div class="menu-nav-container footer">
                <?php 
                    print theme('links',array('links'=>$main_menu));
                ?>              
            </div>
            <p>IPhone selvage et aesthetic aliquip. Lomo eiusmod laboris Bushwick Echo Park pug. Nesciunt iPhone gentrify, dreamcatcher officia delectus Tonx Schlitz bicycle rights nihil Odd Future. Pop-up craft beer sunt, Shoreditch tempor meh Brooklyn fixie asymmetrical sartorial mixtape. IPhone mixtape assumenda synth ad church-key, fanny pack sapiente pour-over squid salvia selfies jean shorts Odd Future sint. Pickled meh sustainable, roof party sriracha meggings vero trust fund McSweeney's eiusmod ethnic craft beer tempor butcher. Kitsch Brooklyn Pitchfork before they sold out, non viral cardigan Godard swag anim consequat voluptate.</p>
        </div>
    </div>
